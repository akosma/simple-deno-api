import { Application, Router } from "https://deno.land/x/oak@v12.1.0/mod.ts"
import { Handlebars } from "https://deno.land/x/handlebars@v0.9.0/mod.ts"
import { Counter, Registry } from "https://deno.land/x/ts_prometheus@v0.3.0/mod.ts"

const app = new Application()
const router = new Router()
const hbs = new Handlebars()
const counter = Counter.with({
  name: "http_requests_total",
  help: "The total HTTP requests",
  labels: ["path", "method", "status"],
})

const DEFAULT_TEXT = `Quam sed autem blanditiis libero sed dolor voluptate optio. Qui quisquam est ratione. Odio et reprehenderit
dolorem dolorum iusto non qui. Consequatur neque iusto est consectetur tempora quisquam temporibus minus.
Vitae ipsa aut voluptatem in qui.`

function getEnvOrDefault(variable: string, defaultValue: string): string {
  const value = Deno.env.get(variable)
  if (value) {
    return value
  }
  return defaultValue
}

function validateNumber(input: string): number {
  const i = parseInt(input, 10)
  if (isNaN(i)) {
    return 1
  }
  return i
}

function randomNumberBetween(min: number, max: number): number {
  return Math.random() * (max - min) + min
}

function log(name: string, message: string, level: string): void {
  const time = (new Date()).toISOString()
  console.log(`[${time}] ${name} ${level.toUpperCase()}: ${message}`)
}

router.get(`/`, async ctx => {
  const baseTextSize = validateNumber(getEnvOrDefault("TEXT_SIZE", "12"))
  const data = {
    "backgroundColor": getEnvOrDefault("BACKGROUND_COLOR", "white"),
    "textColor": getEnvOrDefault("TEXT_COLOR", "black"),
    "titleSize": baseTextSize * 3,
    "textSize": baseTextSize * 2,
    "smallTextSize": baseTextSize,
    "title": getEnvOrDefault("TITLE", "Lorem Ipsum"),
    "text": getEnvOrDefault("TEXT", DEFAULT_TEXT),
    "now": (new Date()).toUTCString(),
    "hostname": Deno.hostname()
  }
  log('simpleapi.root', 'Sending some data', 'INFO')
  ctx.response.body = await hbs.renderView("index", data)
})

router.get(`/healthz`, ctx => {
  const rnd = randomNumberBetween(0, 10)
  const response = `Response: ${rnd}`
  ctx.response.body = response
  if (rnd < 1) {
    ctx.response.status = 500
    log('simpleapi.unstable', response, 'ERROR')
    setTimeout(() => {
      Deno.exit(1)
    }, 200)
  }
  else {
    ctx.response.status = 200
    log('simpleapi.healthz', response, 'INFO')
  }
})

router.get("/metrics", (ctx) => {
  ctx.response.headers.set("Content-Type", "")
  ctx.response.body = Registry.default.metrics()
})

app.use(async (ctx, next) => {
  await next()
  counter.labels({
    path: ctx.request.url.pathname,
    method: ctx.request.method,
    status: String(ctx.response.status || 200),
  }).inc()
})

app.use(router.routes())
console.log(`Now listening on http://0.0.0.0:8080`)
await app.listen("0.0.0.0:8080")
