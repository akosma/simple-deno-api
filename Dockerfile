FROM docker.io/denoland/deno:alpine-1.31.2

WORKDIR /app
COPY ["deno.json", "app.ts", "/app/"]
COPY views /app/views
RUN deno cache app.ts

EXPOSE 8080
USER deno

CMD ["run", "--allow-net", "--allow-env", "--allow-read", "--allow-sys", "app.ts"]
